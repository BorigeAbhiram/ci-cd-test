import datetime
import win32com.client as win32
import argparse

def send_mail(mergereq_title, mergereq_description, commit_message, commit_description):
    # print({"mergereq_title": mergereq_title,
    #        "mergereq_description": mergereq_description,
    #        "commit_message": commit_message,
    #        "commit_description": commit_description
    #        })
    print "***********************"
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.to = 'abhiram.borige@gmail.com;' \
    mail.Subject = f'Gen20x-i2 Automation Master updates as on {datetime.datetime.now()}'
    html_start = f"""
                <html>
                    <body>
                        <p style="font-size:105%">
                        Hi Team, <br><br>
                        Please find the Merge updates here. <br> </p>
                        <h3>{mergereq_title}</h3>
                        <p>
                            {mergereq_description}
                        </p>
                        <p style="font-size:105%">
                        Please find commits here <br> </p>
                        <h3>{commit_message}</h3>
                        <p>{commit_description}</p>
                    </body>
                </html>"""
    mail.HTMLBody = html_start
    mail.Send()

if __name__ == '__main__':
    print "***********************"
    parser = argparse.ArgumentParser(description='Gitlab-CI/CD')
    parser.add_argument("--mergereq_title", required=True, type=str)
    parser.add_argument("--mergereq_description", required=False, default="No description", nargs='?',
                        const="No description", type=str)
    parser.add_argument("--commit_message", required=True, type=str)
    parser.add_argument("--commit_description", required=False, default="No description", nargs='?',
                        const="No description", type=str)
    my_args = vars(parser.parse_args())
    send_mail(
        my_args.get("mergereq_title"),
        my_args.get("mergereq_description"),
        my_args.get("commit_message"),
        my_args.get("commit_description")
    )
    print "Done"
